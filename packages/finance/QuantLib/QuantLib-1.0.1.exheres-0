# Copyright 2009 Ingmar Vanhassel
# Distributed under the terms of the GNU General Public License v2

SUMMARY="A comprehensive software framework for quantitative finance, a library for modeling, trading, and risk management in real-life"
DESCRIPTION="
QuantLib is a cross-platform, quantitative finance C++ library for modeling,
pricing, trading, and risk management in real-life. It is also wrapped as
Python/Ruby/Scheme modules. Extensions for Excel, R, and Mathematica are
available. Other such extensions are under consideration. QuantLib offers tools
that are useful both for practical implementation and for advanced modeling. It
features market conventions, yield curve models, solvers, PDEs, Monte Carlo
(low-discrepancy included), exotic options, VAR, and so on.
"
HOMEPAGE="http://quantlib.org/"
DOWNLOADS="
    mirror://sourceforge/${PN,,}/${PNV}.tar.gz
    doc? ( mirror://sourceforge/${PN,,}/${PN}-docs-${PV}-html.tar.gz )
"
# QuantLibAddin: More procedural API, allows embedding in Excel, OOo calc
# Bindings (using SWIG) for python, perl, ruby, scheme, ocaml, R, java

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="doc"

DEPENDENCIES="
    build+run:
        dev-libs/boost[>=1.34.1]
"

pkg_setup() {
    export EMACS=no
}

src_install() {
    default

    if option doc ; then
        # API docs
        # man(3) pages are also available
        insinto /usr/share/doc/${PNVR}/html/
        doins -r "${WORKBASE}"/${PN}-docs-${PV}-html/*
    fi
}

